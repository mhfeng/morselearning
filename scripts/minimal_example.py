import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage


def optimal_phiH(phi):
    phi_H = np.zeros(phi.shape)

    phi_H[1:-1] = (phi[:-2] + phi[2:])/2
    phi_H[0] = phi[1]
    phi_H[-1] = phi[-2]

    return phi_H


def godunov(phi, a):
    phi_p_x = np.zeros(phi.shape)
    phi_n_x = np.zeros(phi.shape)

    phi_p_x[0:-1] = phi[1:] - phi[0:-1]
    phi_n_x[1:] = phi[1:] - phi[0:-1]

    phi_x_neg = np.maximum(np.power(np.maximum(phi_n_x, 0), 2), np.power(np.minimum(phi_p_x, 0), 2))

    phi_x_pos = np.maximum(np.power(np.minimum(phi_n_x, 0), 2), np.power(np.maximum(phi_p_x, 0), 2))

    phi_x = -np.minimum(np.sign(a), 0) * phi_x_neg + np.maximum(np.sign(a), 0) * phi_x_pos
    return np.sqrt(phi_x)


def sign(phi):
    denom = np.sqrt(np.power(phi, 2) + godunov(phi, np.ones(phi.shape)))
    return np.divide(phi, denom)


def normalize_distance(phi, dt, dx, tol = 1e-6, max_steps = 1e6):
    sign_phi = sign(phi)
    steps = 0
    converged = False
    while not converged:
        dphi = dt * sign_phi * (1 - godunov(phi, -sign_phi))
        phi_new = phi + dphi

        if np.linalg.norm(phi_new - phi) < tol or steps == max_steps:
            converged = True

        steps += 1
        phi = phi_new
    return phi


def first_derivative(phi, dx):
    phi_x = np.zeros(phi.shape)
    phi_x[1:-1] = (phi[2:] - phi[:-2])/(2*dx)
    phi_x[0] = (phi[1]-phi[0])/dx
    phi_x[-1] = (phi[-1]-phi[-2])/dx

    return phi_x

def second_derivative(phi, dx):
    phi_xx = np.zeros(phi.shape)
    phi_xx[1:-1] = (phi[2:] + phi[:-2] - 2*phi[1:-1])/(dx*dx)
    phi_xx[0] = phi_xx[1]
    phi_xx[-1] = phi_xx[-2]

    return phi_xx


if __name__ == '__main__':
    N = int(1e2)
    dx = 1/N
    dt = 1e-2

    test_image = np.ones(N)
    switch_idx = np.linspace(0, N, 10).astype(int)
    for i in np.arange(10):
        if i%2 == 0 and i<9:
            test_image[switch_idx[i]:switch_idx[i+1]] = -1

    normalized_phi = normalize_distance(test_image, dt, dx)
    scaled_phi = normalized_phi/np.max(np.abs(normalized_phi))
    scaled_phi = ndimage.gaussian_filter(normalized_phi, 5)

    plt.plot(scaled_phi)
    plt.savefig('test.png')

