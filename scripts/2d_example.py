import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat


def optimal_phiH(phi):
    phi_H = np.zeros(phi.shape)

    phi_H[1:-1] = (phi[:-2] + phi[2:])/2
    phi_H[0] = phi[1]
    phi_H[-1] = phi[-2]

    return phi_H


def godunov_2d(phi, a):
    phi_p_x = np.zeros(phi.shape)
    phi_n_x = np.zeros(phi.shape)
    phi_p_y = np.zeros(phi.shape)
    phi_n_y = np.zeros(phi.shape)

    phi_p_x[:, 0:-1] = phi[:, 1:] - phi[:, 0:-1]
    phi_n_x[:, 1:] = phi[:, 1:] - phi[:, 0:-1]
    phi_p_y[0:-1, :] = phi[1:, :] - phi[0:-1, :]
    phi_n_y[1:, :] = phi[1:, :] - phi[0:-1, :]

    phi_x_neg = np.maximum(np.power(np.maximum(phi_n_x, 0), 2), np.power(np.minimum(phi_p_x, 0), 2))
    phi_y_neg = np.maximum(np.power(np.maximum(phi_n_y, 0), 2), np.power(np.minimum(phi_p_y, 0), 2))

    phi_x_pos = np.maximum(np.power(np.minimum(phi_n_x, 0), 2), np.power(np.maximum(phi_p_x, 0), 2))
    phi_y_pos = np.maximum(np.power(np.minimum(phi_n_y, 0), 2), np.power(np.maximum(phi_p_y, 0), 2))

    phi_x = -np.minimum(np.sign(a), 0) * phi_x_neg + np.maximum(np.sign(a), 0) * phi_x_pos
    phi_y = -np.minimum(np.sign(a), 0) * phi_y_neg + np.maximum(np.sign(a), 0) * phi_y_pos
    return np.sqrt(phi_x + phi_y)


def sign(phi):
    denom = np.sqrt(np.power(phi, 2) + godunov_2d(phi, np.ones(phi.shape)))
    return np.divide(phi, denom)


def normalize_distance(phi, dt, tol = 1e-2, max_steps = 1000):
    sign_phi = sign(phi)
    steps = 0
    converged = False
    while not converged:
        dphi = dt * sign_phi * (1 - godunov_2d(phi, -sign_phi))
        phi_new = phi + dphi

        if np.linalg.norm(phi_new - phi) < tol or steps == max_steps:
            converged = True

        steps += 1
        phi = phi_new
    return phi


if __name__ == '__main__':
    N = int(1e1)
    dx = 1/N
    dt = 1e-1

    images = loadmat('../Images/piececonst_tau10.mat')
    test_image = (images['imgs'][1,:,:]).astype(int)

    test_image[test_image == 0] = -1

    # plt.imshow(test_image)
    # plt.savefig('test-1.png')

    normalized_phi = normalize_distance(test_image, dt)

    # plt.imshow(normalized_phi)
    # plt.colorbar()
    # plt.savefig('test.png')

